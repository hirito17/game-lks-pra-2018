class Game {
    constructor(player) {
        this.grid = 9;
        this.player = window.localStorage.getItem('player');
        this.maps = [];
    }

    start() {
        this.score = 0;

        for (let r = 0; r < this.grid; r++) {
            this.maps[r] = [];
            for (let c = 0; c < this.grid; c++) {
                this.maps[r][c] = new Gem(r, c, Math.floor(Math.random()*7)+1);
            }
        }

        document.querySelector('#name').innerHTML = window.localStorage.getItem('player');
    }

    render(callback =({tr}) => {}) {
        let score = document.querySelector('#score');
        score.innerHTML = this.score;

        let element = document.querySelector('.table > tbody');
        element.innerHTML = '';

        this.maps.forEach((value, index) => {
            let tr = document.createElement('tr');
            value.forEach((item, key) => {
                tr.appendChild(item.getGem());
            });
            element.appendChild(tr);
        });

        this.check();
        this.checkJewelDestroyed();
    }

    check() {
        this.maps.forEach((value, index) => {
            value.forEach((item, key) => {
                if (item.checkRule()) {
                    game.checkJewelDestroyed();
                    item.check();
                    return false;
                }
                game.checkJewelDestroyed();
            })
        })
    }

    checkJewelDestroyed() {
        for (let column = 0; column < this.grid; column++) {
            do {
                var down = false;
                for (let row = 8; row > 0; row--) {
                    if (this.maps[row][column].image === null) {
                        down = true;
                    }
                    if (down)
                        this.maps[row][column].image = this.maps[row - 1][column].image;
                }
                if (down || this.maps[0][column].image === null) this.maps[0][column].image = new Gem(0, column, Math.floor(Math.random()*7)+1).image;
            }
            while (down);
        }
    }

    destroy() {
        let data = [];
        data['player'] = this.player;
        data['score'] = this.score;

        window.localStorage.setItem('score', JSON.stringify(data));

        return window.location.href = 'ranking.html';
    }
}